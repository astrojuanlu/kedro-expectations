@ECHO OFF
setlocal enabledelayedexpansion

:INSTALL
cd "%~dp0"
echo Installing virtualenv
call python -m pip install virtualenv
IF EXIST "%~dp0venv" (
echo venv already exists
) ELSE (
echo Creating venv
call python -m virtualenv "%~dp0venv" --seeder=pip --python=python3.10
)

ECHO Installing Packages
call venv\Scripts\activate.bat && python -m pip install -r requirements.txt

ECHO Registering kernel for Jupyter notebook
python -m ipykernel install --name reha

SET /P AREYOUSURE=Do you want to install dev requirements, too(Y/[N])?
IF /I "%AREYOUSURE%" NEQ "Y" (GOTO :END) ELSE (GOTO :INSTALL_DEV)

:INSTALL_DEV
ECHO Installing Dev Packages
call venv\Scripts\activate.bat && python -m pip install -r requirements_dev.txt

: END
pause